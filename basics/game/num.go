package main

import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("Welcome to guess a number game!")
	answer := rand.Intn(100) // random number
	var attempt int
	for {
		fmt.Printf("make your guess!\n")
		fmt.Scanf("%d", &attempt) // мы должны сканировать по _адресу_ переменной

		if attempt < answer {
			fmt.Printf("my number is bigger!!\n")
		} else if attempt > answer {
			fmt.Printf("my number is smaller\n")
		} else {
			fmt.Printf("you are right!! congratulations!\n")
			break // exit from cycle
		}
	}
	fmt.Println("goodbye!")
}
