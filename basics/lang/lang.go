package main

import (
	"fmt"
	"strconv"
)

// https://gobyexample.com/
// minumal go:

// variables
// control flow: if, for
// logic
// data types:
// simple data types (int, string, bool)
// complex: array, slice, map
// functions
// (pointers)

// if condition {
// 	// do something
// } else {
// 	// ...
// }

// logical functions
// отрицание !val

func main() {
	t := true
	f := false
	// not !
	// 1 => 0
	// 0 => 1

	fmt.Println("!t =", !t)
	fmt.Println("!f =", !f)
	// or ||
	// or
	// 0 0 => 0
	// 0 1 => 1
	// 1 0 => 1
	// 1 1 => 1
	fmt.Println("t or f=", t || f)
	// and &&
	// 0 0 => 0
	// 0 1 => 0
	// 1 0 => 0
	// 1 1 => 1
	fmt.Println("t and f=", t && f)

	if (1 > 2) || (3 < 4) && true {
		fmt.Println("!")
	}

	// for
	// infinite
	// for {
	// 	fmt.Println("preved!!!")
	// }
	//
	// цикл с условием
	// i := 0
	// for i < 3 {
	// 	fmt.Println(i)
	// 	i++
	// }
	// полная форма
	// var i int = 12

	for i := 0; i < 3; i++ {
		var message string = "hello"
		fmt.Println(message)
		fmt.Println(i)
	}

	// in and out
	// print
	fmt.Println("simple")

	fmt.Printf("hello world!")
	// printf format
	fmt.Printf("hello\tworld!\n")
	// fmt.Printf("backslash: \\")
	// https://gobyexample.com/string-formatting
	fmt.Printf("x = %d name = %s pi = %f\n", 42, "John", 3.1415926)

	var pi float64 = 3.1415
	var name string = "Paul"
	fmt.Printf("%v %v\n", pi, name)

	// fmt.Printf("%v\n", arg) the same fmt.Println(arg)
	// fmt.Println("hello", 1, 2, "Misha", 2.78)

	city := "Moscow    !!!!"
	district := "number 12"
	temp := 16

	fmt.Printf("I live in %10s distict = %s and the temp is %d now.\n", city, district, temp)
	fmt.Println("I live in " + city + " and the temp is " + strconv.Itoa(temp) + " now.\n")

	// in
	userName := ""
	var age int
	fmt.Printf("what is your name?\n")
	// user must enter her name
	fmt.Scanf("%s", &userName)

	fmt.Printf("what is your age?\n")
	fmt.Scanf("%d", &age)

	fmt.Printf("Hello, %s! Nice to see ya! Hope your %d years were happy!\n", userName, age)
}
