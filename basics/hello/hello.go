package main

import "fmt"

func main() {
	// var имя тип = значение
	// var animal string = "cat"
	// animal := "dog"

	// var x int = 42
	// x := 42

	// var y int = 5
	// var pi float64 = 3.1415926
	// var xxx = 100
	// _ = xxx

	// fmt.Println("+", x+y)
	// fmt.Println("-", x-y)
	// fmt.Println("*", x*y)
	// fmt.Println("/", x/y)

	// fmt.Println(x + int(pi))

	// var myVar bool = false
	// f := false

	// condition := (10 != 20)

	var a int = 10
	var b int = 20

	if a > b {
		fmt.Println("a is greater than b!")
	} else {
		fmt.Println("b is greater")
	}
}
