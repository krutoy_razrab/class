package main

import "fmt"

func main() {
	x := 1
	y := 2
	z := 3

	if x >= y && x >= z {
		fmt.Println(x)
	} else if y >= x && y >= z {
		fmt.Println(y)
	} else {
		fmt.Println(z)
	}
}
